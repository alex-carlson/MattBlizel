var gulp = require('gulp'),
	slim = require("gulp-slim"),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	gulpif = require('gulp-if'),
	sprite = require('css-sprite').stream,
	//sourcemaps = require('gulp-sourcemaps'),
	sass = require('gulp-sass'),
	//livereload = require('gulp-livereload'),
	del = require('del');


var paths = {
  scripts: ['_assets/js/**/*.js', '!_assets/vendor/**/*.js'],
  images: '_assets/images/**/*',
  styles: '_assets/styles/**/*',
  slimP: '_assets/slim/**/*',
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function(cb) {
  // You can use multiple globbing patterns as you would with `gulp.src`
  del(['build'], cb);
});

gulp.task('scripts', ['clean'], function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
    //.pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('plugins.v91.js'))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('_public'));
});

//gulp sass
gulp.task('sass', function () {
    gulp.src('_assets/styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('_public'));
		    //.pipe(livereload());
});

gulp.task('slim', function(){
  gulp.src("_assets/slim/*.slim")
    .pipe(slim({
      pretty: true
    }))
	.pipe(gulp.dest(""));
  //.pipe(livereload());
});

// generate sprite.png and _sprite.scss
gulp.task('sprites', function () {
  return gulp.src('_assets/images/sprite/*.png')
    .pipe(sprite({
      name: 'sprite',
      style: '_sprite.scss',
      cssPath: '',
      processor: 'scss'
    }))
    .pipe(gulpif('*.png', gulp.dest('_public'), gulp.dest('_public')))
});

// generate scss with base64 encoded images
gulp.task('base64', function () {
  return gulp.src('_assets/images/sprite/*.png')
    .pipe(sprite({
      base64: true,
      style: '_base64.scss',
      processor: 'scss'
    }))
    .pipe(gulp.dest('_public'));
});

// Copy all static images
gulp.task('images', ['clean'], function() {
  return gulp.src(paths.images)
    // Pass in options to the task
    .pipe(imagemin({optimizationLevel: 5}))
    .pipe(gulp.dest('_public'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  //livereload.listen();
  gulp.watch(paths.scripts, ['scripts']);
  gulp.watch(paths.images, ['images']);
  gulp.watch(paths.styles, ['sass']);
  gulp.watch(paths.slimP, ['slim']);
});

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'images']);
